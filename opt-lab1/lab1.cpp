#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
using namespace std;

typedef struct {
	char value;
	int attr;
}TSymbol;

typedef struct {
	string value;
	int id;
	int line;
	int column;
}lex;

enum errors {
	errBrokenFile,
	errEmptyFile,
	errInvalidChar,
	errCommentStarExpected,
	errCommentNotClosed,
	errCantCreateFile
};


int attributes[128];

int Genline = 1, Gencolumn = 0;

TSymbol getSymbol(FILE *f) {
	TSymbol temp;
	fscanf(f, "%c", &temp.value);
	temp.attr = attributes[temp.value];
	Gencolumn++;
	if (temp.value == '\n') {
		Genline++;
		Gencolumn = 0;
	}
	return temp;
}

void startFill(vector <lex> &key, vector <lex> &idn) {
	lex temp;
	temp.value = "PROCEDURE";
	temp.id = 401;
	key.push_back(temp);
	temp.value = "BEGIN";
	temp.id = 402;
	key.push_back(temp);
	temp.value = "END";
	temp.id = 403;
	key.push_back(temp);

	temp.value = "INTEGER";
	temp.id = 1001;
	idn.push_back(temp);
	temp.value = "FLOAT";
	temp.id = 1002;
	idn.push_back(temp);
	temp.value = "CONST";
	temp.id = 1003;
	idn.push_back(temp);
}

typedef struct {
	bool exists;
	int id;
}returnValue;

returnValue TabSearch(string searchName, vector <lex> table){
	returnValue temp;
	temp.exists = false;
	temp.id = -1;
	for (int i = 0; i < (int)table.size(); i++) {
		if (table[i].value == searchName) {
			temp.exists = true;
			temp.id = table[i].id;
		}
	}
	return temp;
}

void errorOut(int errorCode, int line, int column, FILE *f, string buf = "") {
	if (errorCode == errInvalidChar) {
		fprintf(f, "Lexer error: line %d, column %d: %s is invalid character!\n", line, column, buf.c_str());
	}
	else if (errorCode == errBrokenFile) {
		fprintf(f, "Lexer error: file missing or corrupted!\n");
	}
	else if (errorCode == errCantCreateFile) {
		fprintf(f, "Lexer error: can`t create output file!\n");
	}
	else if (errorCode == errEmptyFile) {
		fprintf(f, "Lexer error: file is empty!\n");
	}
	else if (errorCode == errCommentNotClosed) {
		fprintf(f, "Lexer error: line %d, column %d: comment not closed!\n", line, column);
	}
}

void addLexem(string value, int id, int line, int column, vector <lex> &table) {
	lex newLexem;
	newLexem.id = id;
	newLexem.value = value;
	newLexem.line = line;
	newLexem.column = column;
	table.push_back(newLexem);
}

void outputLexemTable(vector <lex> table, string name, FILE*f, bool lc = false) {
	fprintf(f, "%s\n", name.c_str());

	if (lc) {
		fprintf(f, "%-10s%-5s%-5s%-5s\n", "Lexem", "Id", "Line", "Column");//cout << setw(10) << left << "Lexem" << setw(5) << left << "Id" << setw(5) << left << "Line" << setw(5) << left << "Column" << endl;
		for (int i = 0; i < table.size(); i++) {
			fprintf(f, "%-10s%-5d%-5d%-5d\n", table[i].value.c_str(), table[i].id, table[i].line, table[i].column);
		}
	}

	else {
		fprintf(f, "%-10s%-5s\n", "Lexem", "Id");
		for (int i = 0; i < table.size(); i++) {
			fprintf(f, "%-10s%-5d%\n", table[i].value.c_str(), table[i].id);
		}
	}
	fprintf(f, "\n");
}
int main()
{
	string fileName = "";
	cout << "Insert name of test: ";
	cin >> fileName;
	string generatedfilePath = "tests/" + fileName + "/generated.txt";
	FILE *generated;
	if (!(generated = fopen(generatedfilePath.c_str(), "wt"))) {
		errorOut(errCantCreateFile, 0, 0,generated);
		return -1;
	}
	FILE *f;
	string inputfilePath = "tests/" + fileName + "/input.sig";
	if (!(f=fopen(inputfilePath.c_str(), "rt"))){
		errorOut(errBrokenFile, 0, 0, generated);
		return -1;
	}
	
	vector <lex> constTable;
	vector <lex> keyTable;
	vector <lex> idnTable;
	vector <lex> lexTable;
	startFill(keyTable, idnTable);
	int line, column;
	TSymbol symbol;
	int lexCode;
	string buf;
	for (int i = 0; i < 128; i++) {
		if (i > 7 && i < 11 || i == 13 || i == 32)
			attributes[i] = 0;
		else if (i > 47 && i < 58)
			attributes[i] = 1;
		else if (i > 64 && i < 91) 
			attributes[i] = 2;
		else if (i == 41 || i == 61 || i == 58 || i == 59 || i == 45)
			attributes[i] = 3;
		else if (i == 40)
			attributes[i] = 4;
		else
			attributes[i] = 5;
	}
	symbol = getSymbol(f);
	if (feof(f)) {
		errorOut(errEmptyFile, 0, 0, generated);
	}
	else {
		while (!feof(f)) {
			buf = "";
			lexCode = 0;
			line = Genline;
			column = Gencolumn;
			switch (symbol.attr){
				case 0:
					while (!feof(f)) {
						symbol = getSymbol(f);
						if (symbol.attr != 0) {
							break;
						}
					}
					break;
				case 1:
					while (!feof(f) && symbol.attr == 1) {
						buf += symbol.value;
						symbol = getSymbol(f);
					}
					returnValue tmp = TabSearch(buf, constTable);
					if (tmp.exists) {
						lexCode = tmp.id;
						addLexem(buf, lexCode, line, column, lexTable);
					}
					else {
						lexCode = 500 + (int)constTable.size() + 1;
						addLexem(buf, lexCode, line, column, lexTable);
						addLexem(buf, lexCode, line, column, constTable);
					}
					break;
				case 2:
					while (!feof(f) && (symbol.attr == 1 || symbol.attr == 2)) {
						buf += symbol.value;
						symbol = getSymbol(f);
					}
					tmp = TabSearch(buf, keyTable);
					if (tmp.exists) {
						lexCode = tmp.id;
						addLexem(buf, lexCode, line, column, lexTable);
					}
					else {
						tmp = TabSearch(buf, idnTable);
						if (tmp.exists) {
							lexCode = tmp.id;
							addLexem(buf, lexCode, line, column, lexTable);
						}
						else {
							lexCode = 1000 + (int)idnTable.size() + 1;
							addLexem(buf, lexCode, line, column, lexTable);
							addLexem(buf, lexCode, line, column, idnTable);
						}
					}
					break;
				case 3:
					lexCode = symbol.value;
					buf += symbol.value;
					addLexem(buf, lexCode, line, column, lexTable);
					symbol = getSymbol(f);
					break;
				case 4:
					if (feof(f)) {
						lexCode = symbol.value;
						buf += symbol.value;
						addLexem(buf, lexCode, line, column, lexTable);
						break;
					}
					else {
						int oldLexCode = symbol.value;
						string oldBuf = "";
						oldBuf+=symbol.value;
						int  oldLine = line, oldColumn = column;
						symbol = getSymbol(f);
						if (symbol.value == '*') {
							symbol = getSymbol(f);
							while (symbol.value != ')') {
								while (!feof(f) && symbol.value != '*') {
									symbol = getSymbol(f);
								}
								if (feof(f)) {
									errorOut(errCommentNotClosed, line, column, generated);
									symbol.value = '@';
									break;
								}
								else {
									symbol = getSymbol(f);
								}
							}
							if (!feof(f)) {
								symbol = getSymbol(f);
							}
							break;
						}
						else {
							addLexem(oldBuf, oldLexCode, oldLine, oldColumn, lexTable);
						}
						break;
					}
				case 5:
					buf += symbol.value;
					errorOut(errInvalidChar, line, column, generated, buf);
					symbol = getSymbol(f);
					break;
			}

		}
		outputLexemTable(constTable, "\nTable of constants", generated);
		outputLexemTable(keyTable, "Table of keywords", generated);
		outputLexemTable(idnTable, "Table of identificators", generated);
		outputLexemTable(lexTable, "Table of lexems", generated, true);
	}
	return 0;
}

